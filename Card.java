public class Card {
    private String suit;
    private String value;

    public Card(String suit, String value){
            this.suit = suit;
            this.value = value;	
    }
    public String getsuit(){return this.suit;}		
    public String getvalue(){return this.value;}
    public String toString(){
        return this.value + " of " + this.suit; 
    }
}
