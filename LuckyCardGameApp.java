import java.util.Scanner;
public class LuckyCardGameApp{

	public static void main(String[] args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;	
		Deck myGame = new Deck();
		System.out.println("Welcome!");
		while (manager.getNumberOfCards() > 1 && totalPoints < 5){
		
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			manager.dealCards();
		}
		System.out.println("Final Score: " + totalPoints);
		if (totalPoints < 5){
				System.out.println("You Lose...");
		} else {
				System.out.println("You win!");
		}
		
	}
}
