public class GameManager{
				private Deck drawPile;
				private Card centerCard;
				private Card playerCard;

				public GameManager(){
							this.drawPile = new Deck();
							this.dealCards();


				
				}
				public String toString(){
				return "Center Card:" + centerCard + "\nPlayer Card:" + playerCard;
			 	}
				public void dealCards(){
				this.drawPile.shuffle();
				centerCard = this.drawPile.drawTopCard();
				playerCard = this.drawPile.drawTopCard();
				}
				public int getNumberOfCards(){
				return this.drawPile.length();
				}
				public int calculatePoints(){
					if(centerCard.getvalue().equals(playerCard.getvalue())){
							return 4;
					}else if (centerCard.getsuit().equals(playerCard.getsuit())){
							return 2;
					}else {
							return -1;
					}
				}

}
