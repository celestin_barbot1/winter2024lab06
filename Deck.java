import java.util.Random;
public class Deck {
    private Card[] cards;
    private int numberOfCards;
    private Random rng;

    public Deck(){
        String[] listOfSuits = {"Spades", "Hearts", "Diamonds","Clubs"};
        String[] listOfValues = {"Ace", "Two", "Three","Four","Five","Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
        this.numberOfCards = 52;
        this.rng = new Random();
        this.cards = new Card[52];
				int arrI = 0;
        for(int i=0; i<listOfSuits.length; i++){
            for (int j=0; j<listOfValues.length; j++){
                this.cards[arrI] = new Card(listOfSuits[i], listOfValues[j]);
								arrI++;
            }
            
        }

    }
    public int length(){
        return this.numberOfCards;
    }
    public Card drawTopCard(){
        this.numberOfCards --;
        return this.cards[numberOfCards];
    }
    public String toString(){
        String deck = "";
        for(int i=0; i<this.numberOfCards; i++){
								System.out.println(this.cards[i]);
            deck += this.cards[i].toString() + "\n";
        }
        return deck;
    }

    public void shuffle(){
				Card temp;
        for(int i=0;i<this.numberOfCards-1; i++){
						int rIndex = rng.nextInt(this.numberOfCards-1);			
						temp = this.cards[i];
            this.cards[i] = this.cards[rIndex];
						this.cards[rIndex] = temp;
						
        }
    }



    
 


}
